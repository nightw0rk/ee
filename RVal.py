from math import sin, pi
from datetime import datetime
from time import strftime


class Rval:
    __position = 0
    __cams = []

    def __init__(self):
        self.__position = 0
        self.__cams.append(Cam(45.1))
        self.__cams[0].AssignValve(Valve(40))
        self.__cams.append(Cam(315.1))
        self.__cams[1].AssignValve(Valve(60))

    def rotate(self, angle):
        self.__position = (self.__position + angle ) % 360.0
        for x in self.__cams:
            x.do(self.__position)

    def getCams(self):
        return self.__cams


class Cam:
    __position = 0
    __height = 40
    __length = 42.5
    __valve = object()

    def __init__(self, position):
        self.__position = position

    def AssignValve(self, valve):
        self.__valve = valve

    def do(self, position):
        if (position >= self.__position) and (position <= self.__position + self.__length):
            self.__valve.Open()
                #print "Open", self.__valve.getDiameter(), position * 2 % 360, tick()
        else:
            self.__valve.Close()
                #print "Close", self.__valve.getDiameter(), position * 2 % 360, tick()

    def statusValve(self):
        return self.__valve.getStatus()


class Valve():
    __diameter = 60
    __status = False

    def __init__(self, diameter):
        self.__diameter = diameter
        self.__status = False

    def Open(self):
        if not self.__status:
            self.__status = True
            return True
        else:
            return False

    def Close(self):
        if self.__status:
            self.__status = False
            return True
        else:
            return False

    def getStatus(self):
        return self.__status

    def getDiameter(self):
        return self.__diameter


class Kval():
    __position = 0
    __rval = object()
    __pistons = []
    __stroke = 0

    def __init__(self, rval, stroke):
        self.__rval = rval
        self.__stroke = stroke

    def AssignPiston(self, piston, inputCam, outputCam, posOn):
        piston.AssignInputCam(self.__rval.getCams()[inputCam])
        piston.AssignOutputCam(self.__rval.getCams()[outputCam])
        piston.setPosition(posOn)
        piston.setStroke(self.__stroke)
        piston.AssignSparkPlug(SparkPlug())
        self.__pistons.append(piston)

    def rotate(self, angle):
        self.__position = (self.__position + angle) % 360.0
        self.__rval.rotate(angle / 2)
        for p in self.__pistons:
            p.move(angle)
            p.p()


class Piston():
    __diameter = 0
    __position = 0
    __positionOnKval = 0
    __inputCam = object()
    __outputCam = object()
    __volume = 0
    __volumeTemp = 300
    __volumeP = 101325
    __volumeConst = 0
    __amount = 0
    __amountMol = 0
    __sparkPlug = object()
    __stroke = 0
    __stage = 0
    __Mol = 28.98

    def __init__(self, diameter):
        self.__diameter = diameter

    def setStroke(self, stroke):
        self.__stroke = stroke
        self.__volumeConst = self.__volumeP * (
            ( pi * ((self.__diameter * 0.001) / 2) ** 2 * ((stroke * 0.001) * 2)) ** 1.4)

    def setPosition(self, angle):
        self.__positionOnKval = angle
        self.__position = (sin(angle * (pi / 180)) * self.__stroke) + self.__stroke

    def move(self, delta):
        self.__positionOnKval = (self.__positionOnKval + delta) % 360.0
        deltaMove = sin(self.__positionOnKval * (pi / 180))
        self.__position = (deltaMove * self.__stroke) + self.__stroke
        if self.__position == 0:
            self.__stage += 1
            #print "DDP", self.__positionOnKval, tick(), self.__stage
        if self.__position == self.__stroke * 2:
            #print "TDP", self.__positionOnKval, tick(), (self.__stage + 1) % 4
            if self.__stage==2:
                self.__sparkPlug.spark()
            self.__stage = (self.__stage + 1) % 4
        deltaVolume = pi * (self.__diameter / 2) ** 2 * ((self.__stroke * 2) - self.__position)
        if self.__stage == 1:
            self.__volume = deltaVolume * 0.0000001
            self.__amount = deltaVolume * 1.225
        elif self.__stage == 2:
            self.__volume = deltaVolume * 0.0000001
            self.__volumeTemp = self.__volumeConst / (self.__volume ** -0.4)
            self.__volumeP = self.__volumeConst / (self.__volume ** 1.4)
    def p(self):
        print self.__volume,";",self.__volumeP
    def AssignInputCam(self, cam):
        __inputCam = cam

    def AssignOutputCam(self, cam):
        __outputCam = cam

    def AssignSparkPlug(self, sparkPlug):
        self.__sparkPlug = sparkPlug


class  SparkPlug:
    def spark(self):
        a=1+1
        #print "Spark", tick()

startTime = datetime.now()

def tick():
    global startTime
    el = datetime.now() - startTime
    return '{0}.{1}'.format(el.seconds, el.microseconds)

rv = Rval()
k = Kval(rv, 140)
p = Piston(80)
k.AssignPiston(p, 0, 1, 0)
for x in range(0, 3600):
    k.rotate(0.5)